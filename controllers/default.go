package controllers

import (
	"gitee.com/owencz/oneNetGo"
	// "github.com/ascoders/alipay"
	"oneServer/models"
	"encoding/json"
	"github.com/astaxie/beego"
)

type MainController struct {
	beego.Controller
}

func (this *MainController) Get() {
	this.Data["Website"] = "beego.me"
	this.Data["Email"] = "astaxie@gmail.thisom"
	this.TplName = "index.html"


	// options := alipay.Options{
	// 	OrderId:  "123",
	// 	Fee:      99.8,
	// 	NickName: "翱翔大空",
	// 	Subject:  "充值100",
	// }

	// alipay := alipay.Client{
	// 	Partner:   "", // 合作者ID
	// 	Key:       "", // 合作者私钥
	// 	ReturnUrl: "", // 同步返回地址
	// 	NotifyUrl: "", // 网站异步返回地址
	// 	Email:     "", // 网站卖家邮箱地址
	// }
	// form := alipay.Form(options)
	// beego.Debug(form)
	// this.Ctx.WriteString(form)

}


type UserController struct {
	beego.Controller
}


/**
 * 用户登入结构体
 */
type UserCredentials struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

func (this *UserController) Login(){
	var  user UserCredentials
	json.Unmarshal(this.Ctx.Input.RequestBody, &user)
	beego.Debug("login with :",user.Username,user.Password)
	if models.CheckUserLogin(user.Username,user.Password){		
		beego.Debug("login success with :",user.Username,user.Password)		
		tokanString:= models.GenToken()
		beego.Debug("tokanString:",tokanString)
		this.Ctx.ResponseWriter.Write([]byte(tokanString))
	}else{
		beego.Debug("login fail with :",user.Username,user.Password)
		this.Ctx.ResponseWriter.Write([]byte("login fail"))
	}	
}

/**
 * 用户注册
 * 
*/
func (this *UserController) Register(){
	beego.Debug("this is Register")
	this.Ctx.ResponseWriter.Write([]byte("Register success"))
	 
}

/**
 * 用户删除
*/
func (this *UserController) Delete(){
	beego.Debug("this is delete")
	this.Ctx.ResponseWriter.Write([]byte("delete success"))
}

/**
 * 设备查询
*/
type DeviceController struct {
	beego.Controller
}

func (this *DeviceController) SearchDevice(){
	beego.Debug("this is searchDevice")
	this.Ctx.ResponseWriter.Write([]byte("this is searchDevice"))
}

/**
 * oneNet数据转发到服务器，并保存到数据库
*/
type TransferController struct{
	beego.Controller
}

func (this *TransferController) TransferOne()  {

	msg,err:=oneNetGo.GetEdpTransferMsg(this.Ctx.ResponseWriter, this.Ctx.Request) //解析oneNet的报文消息
	if err == oneNetGo.TypeFail {
		beego.Debug("transferone is fail")
	}else if err == oneNetGo.TypeVerify {
		beego.Debug("transfer is check connet")
	}else {
		beego.Debug("transferone is msg")	
		beego.Debug("transferOne return ok")	
		data,_:=json.Marshal(msg)
		models.InsertDeviceMsg(msg.Msg.DevID,string(data))	//数据写入数据库	
		
		//主动发送报文给设备端,需要apikey和设备id,数据发送给oneNet,oneNet自动查找设备并发送
		var urlparam  oneNetGo.UrlEdpParam
		urlparam.Device_id = msg.Msg.DevID
		urlparam.Qos = 0
		urlparam.Timeout = 300
		urlparam.Cmd_type = 0
		handers := oneNetGo.HttpHanders{}
		handers.ApiKey = "mF4hN1PsLPqsPRm=h=hwz7S6mdY="
		handers.ContentType = "application/json"	
		oneNetGo.PushData(urlparam, handers, "{\"cmd\":\"hello, I'm server\"}")	//下发消息给oneNet,oneNet会自动设备
		beego.Debug("transferone pushdata finish")	
		
		this.Abort("200")		//http 直接应答200 ok
	}
	
}



/**
*阿里支付的方法
*/
type PayContorller struct{
	beego.Controller
}

func (this *PayContorller) AliPay() {
	beego.Debug("this is alipay")
	models.QRCreate("http://www.baidu.com")		//创建二维码
	this.Abort("200")

}