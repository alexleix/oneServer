/*
 * @Author: owen 
 * @Date: 2017-12-06 11:27:19 
 * @Last Modified by: owen
 * @Last Modified time: 2017-12-06 11:34:29
 * 二维码自动生成并保存
 * 二维码大小设置成360的正方形
 */

package models

import (
	"log"
	"strings"
	"github.com/astaxie/beego"
	"github.com/skip2/go-qrcode"
	"path/filepath"  
	"os"
)
const (
	file_path string = "E:/work/myWork/goWork/src/oneServer/static/qr"
)

func QRCreate(url string){
	// file_path := getParentDirectory(getCurrentDirectory())
	beego.Debug("qr file path:",file_path)
	err:=qrcode.WriteFile(url,qrcode.High,360, file_path+"test"+".png")	//大小为360*360
	if err != nil {
		beego.Debug("qr create error:",err)
	}else{
		beego.Debug("qr create success")
	}
}



func substr(s string, pos, length int) string {  
    runes := []rune(s)  
    l := pos + length  
    if l > len(runes) {  
        l = len(runes)  
    }  
    return string(runes[pos:l])  
}  
  
func getParentDirectory(dirctory string) string {  
    return substr(dirctory, 0, strings.LastIndex(dirctory, "/"))  
}  
  
func getCurrentDirectory() string {  
    dir, err := filepath.Abs(filepath.Dir(os.Args[0]))  
    if err != nil {  
        log.Fatal(err)  
    }  
    return strings.Replace(dir, "\\", "/", -1)  
} 