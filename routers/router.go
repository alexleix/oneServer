package routers

import (
	"github.com/astaxie/beego"
	"oneServer/controllers"
)

func init() {
	beego.Router("/", &controllers.MainController{})
	beego.Router("/user/login",&controllers.UserController{},"*:Login")
	beego.Router("/user/register",&controllers.UserController{},"*:Register")
	beego.Router("/user/delete",&controllers.UserController{},"*:Delete")
	beego.Router("/api/search/device",&controllers.DeviceController{},"*:SearchDevice")
	beego.Router("/transferone", &controllers.TransferController{},"*:TransferOne")
	beego.Router("/aliPay", &controllers.PayContorller{}, "*:AliPay")

}
