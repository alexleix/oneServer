package main

import (
	"oneServer/models"
	"net/http"
	_ "oneServer/routers"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/context"
)

func main() {
	//路由过滤器
	beego.InsertFilter("/api/*", beego.BeforeRouter, func (ctx *context.Context)  {
		beego.Debug("check router:beego.InsertFilter")
		// 把tokan放在http hander中，从key为tokan中取出
		tokanString := ctx.Request.Header.Get("tokan")
		if !models.CheckToken(tokanString){
			beego.Debug("tokan is unavild")
			http.Redirect(ctx.ResponseWriter, ctx.Request, "/", http.StatusMovedPermanently)
		}
	})
	models.MysqlInit()
	beego.Run()
}


